package daniz.github.startover.repository;

import daniz.github.startover.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
